const LandingConfig = {
    className: {
        landingPage: 'landingPageWrapper',
        headerOptions: 'header__options',
        headerSelectTitle: 'header__select--title',
        headerOptionsHidden: 'header__options--hidden',
        headerSelectOption: 'header__select--option',
        headerButton: 'header__button',
        mainContent: 'main__contentWrapper',
        pictureRabbit: 'main__smallPicture--rabbit',
        pictureRabbitActive: 'main__smallPicture--active'
    },

    idName: {
        selectGender: 'select-gender', 
        selectPrice: 'select-price', 
        selectCategory: 'select-category' 
    }
  };
  
  
  const LandingPage = (() => {
    const { className } = LandingConfig;
    const { idName } = LandingConfig;
    const GENDER = document.getElementById(`${idName.selectGender}`);
    const PRICE = document.getElementById(`${idName.selectPrice}`);
    const CATEGORY = document.getElementById(`${idName.selectCategory}`);
    const LANDING_PAGE = document.querySelector(`.${className.landingPage}`);
    const SELECT_TITLE = document.querySelectorAll(`.${className.headerSelectTitle}`);
    const OPTIONS = document.querySelectorAll(`.${className.headerOptions}`);
    const SINGLE_OPTION = document.querySelectorAll(`.${className.headerSelectOption}`);
    const HEADER_BUTON = document.querySelector(`.${className.headerButton}`);
    const MAIN_CONTENT = document.querySelector(`.${className.mainContent}`);
    const RABBIT = document.querySelector(`.${className.pictureRabbit}`);

    const hasRabbitClass = RABBIT.classList.contains(`.${className.pictureRabbitActive}`);
    const mainURL = 'https://www.yves-rocher.pl/';
    
    /**
     * InterSection Observer - można użyć ale za mało sekcji
     */
    
    // let options = {
    //     threshold: .4,
    // }

    // const moveRabbit = entries => {
    //     entries.forEach(entry => {
    //         if (!hasRabbitClass && entry.isIntersecting) {
    //             RABBIT.classList.add(`${className.pictureRabbitActive}`)
    //         } else {
    //             RABBIT.classList.remove(`${className.pictureRabbitActive}`);
    //         }
    //     })
    // }

    // const loadObserver = () => {
    //     let observer = new IntersectionObserver(moveRabbit, options);
    //     observer.observe(LANDING_PAGE);
    // }

    const showOptions = e => {
        if (e.target.id) {
            const clickedButtonOptions = document.querySelector(`[data-id=${e.target.id}]`);
            
            if (clickedButtonOptions.classList.contains(className.headerOptionsHidden)) {
                [].slice.call(OPTIONS).forEach(el => el.classList.add(className.headerOptionsHidden));
                clickedButtonOptions.classList.remove(className.headerOptionsHidden);
            } else {
                clickedButtonOptions.classList.add(className.headerOptionsHidden);
            }
        } else {
            [].slice.call(OPTIONS).forEach(el => el.classList.add(className.headerOptionsHidden));
        }
    }

    const createUrl = () => {
        const gender = GENDER.dataset.value ? encodeURIComponent(GENDER.dataset.value) : 'default';
        const price = PRICE.dataset.value ? encodeURIComponent(PRICE.dataset.value) : 'default';
        const category = CATEGORY.dataset.value ? encodeURIComponent(CATEGORY.dataset.value) : 'default';

        HEADER_BUTON.setAttribute('href', `${mainURL}#prezent&gender=${gender}&price=${price}&category=${category}`);
    }

    const chooseValue = e => {
        const optionText = e.target.innerText;
        const optionValue = e.target.dataset.value;
        const buttonTitle = e.target.dataset.id;
        const button = document.getElementById(buttonTitle);
        
        button.innerText = optionText;
        button.dataset.value = optionValue;
        showOptions(e);
        createUrl(e);
    }

    const moveRabit = () => {
        if (!hasRabbitClass && MAIN_CONTENT.getClientRects()[0].top < window.innerHeight && MAIN_CONTENT.getClientRects()[0].bottom > window.innerHeight) {
                RABBIT.classList.add(`${className.pictureRabbitActive}`)
            } else {
                RABBIT.classList.remove(`${className.pictureRabbitActive}`);
            }
    }
    
    const init = () => {
        if (LANDING_PAGE) {
                window.addEventListener('scroll', moveRabit);                
        }

        if (SELECT_TITLE) {
            [].slice.call(SELECT_TITLE).forEach(el => el.addEventListener('click', showOptions));
        }

        if (SINGLE_OPTION) {
            [].slice.call(SINGLE_OPTION).forEach(el => el.addEventListener('click', chooseValue))
        }

        if(HEADER_BUTON) {
            HEADER_BUTON.addEventListener('click', createUrl);
        }

    };
  
    return { init };
  })();
  
  window.addEventListener('load', () => {
    LandingPage.init();
  });
   