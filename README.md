** Task made for job application purposes

** Simply One Page website with dynamic URL building 

** made with Vanilla.js / HTML5 / CSS (BEM METODOLOGY) / mobile first RWD

** Overview: 

As a job applicant I was asked to build a website from scratch including views for mobile and desktop only, with no framework for JS and no external libraries as well.